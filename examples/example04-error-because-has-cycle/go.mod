module gitlab.com/lesql/dag/examples/example03-one-soft-fail

replace gitlab.com/lesql/dag => ../../

go 1.17

require (
	github.com/sirupsen/logrus v1.9.0
	gitlab.com/lesql/dag v0.0.0-00010101000000-000000000000
)

require (
	filippo.io/mostly-harmless/cryptosource v0.0.0-20220729104653-a94493175ac6 // indirect
	github.com/eknkc/basex v1.0.1 // indirect
	github.com/oklog/ulid/v2 v2.1.0 // indirect
	golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab // indirect
)
