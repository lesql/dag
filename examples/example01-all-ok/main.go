package main

// All DAG jobs finish successfully

// Using ZAP as logger. See example 3 how to use LOGRUS

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/lesql/dag"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func NewLogger() (*zap.Logger, zap.Config) {
	encoderConfig := zapcore.EncoderConfig{
		TimeKey:    "time",
		LevelKey:   "level",
		MessageKey: "event",
		NameKey:    "nameKey",
		//EncodeLevel: zapcore.LowercaseLevelEncoder, //Should be used with JSON
		EncodeLevel: zapcore.CapitalColorLevelEncoder,
		EncodeTime:  zapcore.ISO8601TimeEncoder,
		//EncodeTime:     TimeEncoder, //Write our own time encoder
		EncodeDuration: zapcore.SecondsDurationEncoder,
		LineEnding:     zapcore.DefaultLineEnding, //The default line break, even if it is not set
		// EncodeLevel:    zapcore.LowercaseLevelEncoder,//lower case
		// EncodeTime:     zapcore.EpochTimeEncoder,//Time is changed to s
		// EncodeDuration: zapcore.SecondsDurationEncoder,//Date to s
		// EncodeCaller:   zapcore.ShortCallerEncoder,//Record the calling path format as package/file:line

	}
	atomicLevel := zap.NewAtomicLevel()
	atomicLevel.SetLevel(zap.InfoLevel) // default log level: info

	loggerConfig := zap.Config{
		Level:            atomicLevel,
		Development:      true,
		Encoding:         "console", //json as alternative
		EncoderConfig:    encoderConfig,
		OutputPaths:      []string{"stdout"},
		ErrorOutputPaths: []string{"stderr"}, // OutputPaths:      []string{"stderr", "./log.err"},
	}

	zapLogger, err := loggerConfig.Build()
	if err != nil {
		panic(err)
	}

	return zapLogger, loggerConfig
}

func main() {

	logger, _ := NewLogger()
	mylogger := logger.Sugar()
	myloggerDag := logger.Sugar()

	dagHooks := dag.Hooks{
		DagCreated: func(dag *dag.Dag) {
			mylogger.Infof("DagCreated: %s - %s ", dag.Name, dag.ID)
		},
		DagStarted: func(dag *dag.Dag) {
			mylogger.Infoln("DagStarted")
		},
		DagFinishedSuccess: func(dag *dag.Dag) {
			mylogger.Infoln("DagFinishedSuccess")
		},
		DagFinishedFailed: func(dag *dag.Dag) {
			mylogger.Errorln("DagFinishedFailed")
		},
		JobCreated: func(dagJob *dag.Job) {
			mylogger.Infoln("JobCreated")
		},
		JobStarted: func(dagJob *dag.Job) {
			mylogger.Infof("JobStarted: %s - %s", dagJob.Name, dagJob.ID)
		},
		JobFinishedSuccess: func(dagJob *dag.Job) {
			mylogger.Infof("JobFinishedSuccess: %s - %s", dagJob.Name, dagJob.ID)
		},
		JobFinishedFailed: func(dagJob *dag.Job) {
			mylogger.Errorf("JobFinishedFailed: %s - %s", dagJob.Name, dagJob.ID)
		},
	}

	dag, err := dag.New("Dag 1", myloggerDag, dagHooks)
	if err != nil {
		mylogger.Errorln(err)
		os.Exit(1)
	}

	job1, err := dag.CreateJob("Task 1", func() (map[string]string, error) {
		fmt.Println("Running task 1: sleep 12s")
		time.Sleep(12 * time.Second)
		ret := make(map[string]string)
		return ret, nil
	})

	if err != nil {
		mylogger.Errorln(err)
		os.Exit(1)
	}

	job2, err := dag.CreateJob("Task 2", func() (map[string]string, error) {
		fmt.Println("Running task 2: sleep 10s")
		time.Sleep(10 * time.Second)
		ret := make(map[string]string)
		return ret, nil
	})

	if err != nil {
		mylogger.Errorln(err)
		os.Exit(1)
	}

	job3, err := dag.CreateJob("Task 3", func() (map[string]string, error) {
		fmt.Println("Running task 3: sleep 5s")
		time.Sleep(5 * time.Second)
		ret := make(map[string]string)
		return ret, nil
	})

	if err != nil {
		mylogger.Errorln(err)
		os.Exit(1)
	}

	err = job1.Then(job3)
	if err != nil {
		mylogger.Errorln(err)
		os.Exit(1)
	}

	err = job2.Then(job3)
	if err != nil {
		mylogger.Errorln(err)
		os.Exit(1)
	}

	mylogger.Infoln("This jobs will run:")
	jobs, err := dag.GetJobs()
	if err != nil {
		mylogger.Errorln(err)
		os.Exit(1)
	}

	for _, job := range jobs {
		mylogger.Infoln(job.Name)
	}
	mylogger.Infoln("Starting DAG:")

	err = dag.Run()
	if err != nil {
		mylogger.Errorln(err)
		os.Exit(1)
	}

}
