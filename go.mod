module gitlab.com/lesql/dag

go 1.17

require (
	filippo.io/mostly-harmless/cryptosource v0.0.0-20220729104653-a94493175ac6
	github.com/eknkc/basex v1.0.1
	github.com/oklog/ulid/v2 v2.1.0
	github.com/sirupsen/logrus v1.9.0
	github.com/stretchr/testify v1.7.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
