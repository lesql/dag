package dag

import (
	"fmt"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

var (
	loggerDag *logrus.Entry
	dagHooks  Hooks
)

func init() {
	loggr := logrus.New()
	loggerDag = loggr.WithFields(logrus.Fields{
		"xyz": "xyz",
	})
	dagHooks = Hooks{
		DagCreated: func(dag *Dag) {
			//logrus.Infof("DagCreated: %s - %s ", dag.Name, dag.ID)
		},
		DagStarted: func(dag *Dag) {
			// logrus.Infoln("DagStarted")
		},
		DagFinishedSuccess: func(dag *Dag) {
			// logrus.Infoln("DagFinishedSuccess")
		},
		DagFinishedFailed: func(dag *Dag) {
			// logrus.Errorln("DagFinishedFailed")
		},
		JobCreated: func(dagJob *Job) {
			// logrus.Infoln("JobCreated")
		},
		JobStarted: func(dagJob *Job) {
			// logrus.Infof("JobStarted: %s - %s", dagJob.Name, dagJob.ID)
		},
		JobFinishedSuccess: func(dagJob *Job) {
			// logrus.Infof("JobFinishedSuccess: %s - %s", dagJob.Name, dagJob.ID)
		},
		JobFinishedFailed: func(dagJob *Job) {
			// logrus.Errorf("JobFinishedFailed: %s - %s", dagJob.Name, dagJob.ID)
		},
	}
}

func TestDagNew(t *testing.T) {
	var err error

	dag, jobs, err := createDagAndJobs("TestDagNew")
	assert.Nil(t, err)
	_ = jobs
	assert.IsType(t, dag, &Dag{})
	assert.Equal(t, "DAG_TestDagNew", dag.Name)
	assert.Equal(t, DagStatusNotStarted, dag.Status)
	assert.NotNil(t, dag.ID)
}

func TestCreateJob(t *testing.T) {

	dag, jobs, err := createDagAndJobs("TestCreateJob")
	assert.Nil(t, err)
	_ = dag

	job1 := jobs["job1"]

	assert.Nil(t, err)
	assert.IsType(t, job1, &Job{})
	assert.Equal(t, "Task 1", job1.Name)
	assert.Equal(t, JobStatusNotStarted, job1.Status)
	assert.Equal(t, 0, len(job1.ParentJobs))
	assert.Equal(t, 0, len(job1.ChildJobs))
	assert.Equal(t, 0, len(job1.Data))
	assert.Equal(t, 0, len(job1.Responses))

}

func TestThen(t *testing.T) {

	dag, jobs, err := createDagAndJobs("TestThen")
	assert.Nil(t, err)
	_ = dag

	job1 := jobs["job1"]
	job2 := jobs["job2"]

	err = job1.Then(job2)
	assert.Nil(t, err)

	assert.Equal(t, "Task 2", job1.ChildJobs[0].Name)

}

func TestAfter(t *testing.T) {

	dag, jobs, err := createDagAndJobs("TestAfter")
	assert.Nil(t, err)
	_ = dag

	job1 := jobs["job1"]
	job2 := jobs["job2"]

	err = job2.After(job1)
	assert.Nil(t, err)

	assert.Equal(t, "Task 2", job1.ChildJobs[0].Name)

}

func TestDetectCycleInThen(t *testing.T) {

	dag, jobs, err := createDagAndJobs("TestDetectCycleInThen")
	assert.Nil(t, err)
	_ = dag

	job1 := jobs["job1"]
	job2 := jobs["job2"]

	err = job1.Then(job2)
	assert.Nil(t, err)
	err = job2.Then(job1)
	assert.NotNil(t, err)
}
func TestDetectCycleInAfter(t *testing.T) {

	dag, jobs, err := createDagAndJobs("TestDetectCycleInAfter")
	assert.Nil(t, err)
	_ = dag

	job1 := jobs["job1"]
	job2 := jobs["job2"]

	err = job1.After(job2)
	assert.Nil(t, err)
	err = job2.After(job1)
	assert.NotNil(t, err)

}

func TestGetJob(t *testing.T) {

	dag, jobs, err := createDagAndJobs("TestGetJob")
	assert.Nil(t, err)

	job1 := jobs["job1"]
	job2 := jobs["job2"]

	err = job1.Then(job2)
	assert.Nil(t, err)

	job1Return, err := dag.GetJob("Task 1")
	assert.Nil(t, err)

	assert.Equal(t, "Task 1", job1Return.Name)
	assert.Equal(t, JobStatusNotStarted, job1Return.Status)
	assert.Equal(t, 0, len(job1Return.ParentJobs))
	assert.Equal(t, 1, len(job1Return.ChildJobs))
	assert.Equal(t, 0, len(job1Return.Data))
	assert.Equal(t, 0, len(job1Return.Responses))
}

func TestGetJobs(t *testing.T) {

	dag, jobs, err := createDagAndJobs("TestGetJobs")
	assert.Nil(t, err)
	job1 := jobs["job1"]
	job2 := jobs["job2"]
	job3 := jobs["job3"]

	err = job2.After(job1)
	assert.Nil(t, err)

	err = job2.Then(job3)
	assert.Nil(t, err)

	jobsRet, err := dag.GetJobs()
	assert.Nil(t, err)

	assert.IsType(t, jobsRet, []JobBasics{})
	assert.Equal(t, 3, len(jobsRet))

	assert.Equal(t, "Task 1", jobsRet[0].ID)
	assert.Equal(t, "Task 2", jobsRet[1].ID)
	assert.Equal(t, "Task 3", jobsRet[2].ID)

	assert.Equal(t, "Task 1", jobsRet[0].Name)
	assert.Equal(t, "Task 2", jobsRet[1].Name)
	assert.Equal(t, "Task 3", jobsRet[2].Name)

	assert.Equal(t, 0, jobsRet[0].ParentJobsCount)
	assert.Equal(t, 1, jobsRet[1].ParentJobsCount)
	assert.Equal(t, 1, jobsRet[2].ParentJobsCount)

}

func TestGetJobEdges(t *testing.T) {

	dag, jobs, err := createDagAndJobs("TestGetJobEdges")
	assert.Nil(t, err)
	job1 := jobs["job1"]
	job2 := jobs["job2"]
	job3 := jobs["job3"]

	err = job2.After(job1)
	assert.Nil(t, err)

	err = job2.Then(job3)
	assert.Nil(t, err)

	jobEdges, err := dag.GetJobEdges()
	assert.Nil(t, err)
	assert.Equal(t, "Task 1", jobEdges[0].From)
	assert.Equal(t, "Task 2", jobEdges[0].To)

	assert.Equal(t, "Task 3", jobEdges[1].To)
	assert.Equal(t, "Task 2", jobEdges[1].From)

}

func TestRun(t *testing.T) {

	dag, jobs, err := createDagAndJobs("TestRun")
	assert.Nil(t, err)
	job1 := jobs["job1"]
	job2 := jobs["job2"]
	job3 := jobs["job3"]

	err = job2.After(job1)
	assert.Nil(t, err)

	err = job2.Then(job3)
	assert.Nil(t, err)

	err = dag.Run()
	assert.Nil(t, err)

	assert.IsType(t, &Dag{}, dag)
	assert.Equal(t, dag.Name, "DAG_TestRun")
	assert.Equal(t, dag.Status, DagStatusFinishedSuccess)
	assert.NotNil(t, dag.ID)

}

func createDagAndJobs(dagName string) (*Dag, map[string]*Job, error) {
	dag, err := New("DAG_"+dagName, loggerDag, dagHooks)
	if err != nil {
		return dag, map[string]*Job{}, err
	}

	//fmt.Println(dag.Name, &dag)

	var jobs = map[string]*Job{}

	jobs["job1"], err = dag.CreateJob("Task 1", func() (map[string]string, error) {
		fmt.Println("Running task 1: sleep 0.5s")
		time.Sleep(500) //1 * time.Second)
		ret := make(map[string]string)
		return ret, nil
	})
	if err != nil {
		return dag, map[string]*Job{}, err
	}

	jobs["job2"], err = dag.CreateJob("Task 2", func() (map[string]string, error) {
		fmt.Println("Running task 2: sleep 0.5s")
		time.Sleep(500) //1 * time.Second)
		ret := make(map[string]string)
		return ret, nil
	})
	if err != nil {
		return dag, map[string]*Job{}, err
	}

	jobs["job3"], err = dag.CreateJob("Task 3", func() (map[string]string, error) {
		fmt.Println("Running task 3: sleep 0.5s")
		time.Sleep(500) //1 * time.Second)
		ret := make(map[string]string)
		return ret, nil
	})
	if err != nil {
		return dag, map[string]*Job{}, err
	}

	return dag, jobs, nil
}
