package dag

import (
	"errors"
	"time"
)

type Edge struct {
	From string `json:"from"`
	To   string `json:"to"`
}

type JobBasics struct {
	ID              string `json:"id"`
	Name            string `json:"name"`
	ParentJobsCount int    `json:"parentJobsCount"`
}

func runJob(job *Job) {

	job.Logger.Debugf("=> %s Starting", job.Name)
	job.Mu.Lock()
	job.Status = JobStatusRunning
	job.StartedTs = time.Now()
	job.Mu.Unlock()

	hooks.JobStarted(job)

	job.Mu.Lock()
	responses, err := job.task()
	job.FinishedTs = time.Now()
	job.Responses = responses
	job.Mu.Unlock()
	if err != nil {
		if responses["SOFT_ERROR"] != "SOFT_ERROR" {
			job.Mu.Lock()
			job.Status = JobStatusFinishedWithSoftError
			job.Mu.Unlock()
			job.Logger.Debugf("=> %s Finished with errors => Soft error => %s", job.Name, job.Status)
			hooks.JobFinishedFailed(job)
			return
		}
		job.Mu.Lock()
		job.Status = JobStatusFinishedWithError
		job.Mu.Unlock()
		job.Logger.Debugf("=> %s Finished with errors %s", job.Name, job.Status)
		hooks.JobFinishedFailed(job)
		return
	}

	if responses["CONDITIONS_NOT_MET"] != "" {
		job.Mu.Lock()
		job.Status = JobStatusFinishedConditionNotMet
		job.Mu.Unlock()
		job.Logger.Debugf("=> %s Conditions not met => %s", job.Name, job.Status)
		hooks.JobFinishedSuccess(job)
		return
	}

	job.Mu.Lock()
	job.Status = JobStatusFinishedSuccess
	job.Mu.Unlock()
	job.Logger.Debugf("=> %s Finished => %s", job.Name, job.Status)
	hooks.JobFinishedSuccess(job)
}

func failJob(job *Job) {
	job.Mu.Lock()
	job.StartedTs = time.Now()
	job.Status = JobStatusFinishedWithErrorAsItHasParentsHaveErrors
	job.FinishedTs = time.Now()
	job.Mu.Unlock()

	job.Logger.Debugf("=> %s: Failing job %s", job.Name, job.Status)
	hooks.JobFinishedFailed(job)
}

// JobExists tests if job exists
func (dag *Dag) JobExists(name string) bool {
	if _, ok := dag.Jobs[name]; ok {
		return true
	}
	return false
}

// CreateJob adds a job to DAG
func (dag *Dag) CreateJob(name string, task func() (map[string]string, error)) (*Job, error) {

	if _, ok := dag.Jobs[name]; ok {

		return &Job{}, errors.New("Job with this name already exists: '" + name + "'")

	}

	ID := randStringUlid()

	job := &Job{
		ID:        ID,
		Name:      name,
		task:      task,
		DagID:     dag.ID,
		Status:    JobStatusNotStarted,
		CreatedTs: time.Now(),
		Logger:    dag.Logger,
	}

	job.Data = make(map[string]string)
	job.Responses = make(map[string]string)

	dag.mu.Lock()
	dag.Jobs[name] = job
	dag.JobsOrder = append(dag.JobsOrder, name)
	dag.mu.Unlock()
	hooks.JobCreated(job)

	return job, nil
}

// AddData adds additional data to job
func (job *Job) AddData(key string, value string) {
	job.Mu.Lock()
	job.Data[key] = value
	job.Mu.Unlock()
}

// GetJobs returns a list of job names in order as they were added
func (dag *Dag) GetJobs() ([]JobBasics, error) {

	jobs := []JobBasics{}

	for _, jobName := range dag.JobsOrder {

		job, err := dag.GetJob(jobName)

		if err != nil {
			return []JobBasics{}, err
		}

		jobBasics := JobBasics{
			ID:              jobName,
			Name:            jobName,
			ParentJobsCount: len(job.ParentJobs),
		}

		jobs = append(jobs, jobBasics)
	}
	return jobs, nil
}

// GetJob returns a job
func (dag *Dag) GetJob(name string) (*Job, error) {
	if _, ok := dag.Jobs[name]; ok {
		return dag.Jobs[name], nil
	}
	return &Job{}, errors.New("job with name '" + name + "' does not exist")
}

// GetJobEdges returns a list of job names
func (dag *Dag) GetJobEdges() ([]Edge, error) {

	edges := []Edge{}

	for jobName, job := range dag.Jobs {

		for _, childJob := range job.ChildJobs {
			edge := Edge{
				From: jobName,
				To:   childJob.Name,
			}
			edges = append(edges, edge)
		}
	}
	return edges, nil
}

func (dag *Dag) ShowJobStatuses() error {
	for _, job := range dag.Jobs {
		job.Logger.Infof("%s: %s - %s - %s", job.Name, job.StartedTs.Format("2006-01-02 15:04:05 -0700 MST"), job.FinishedTs.Format("2006-01-02 15:04:05 -0700 MST"), job.Status)
	}
	return nil
}

func (job *Job) After(parent *Job) error {

	if job.DagID != parent.DagID {
		return errors.New("cannot combine jobs of different DAGS")
	}

	parent.Mu.Lock()
	parent.ChildJobs = append(parent.ChildJobs, job)
	parent.Mu.Unlock()

	job.Mu.Lock()
	job.ParentJobs = append(job.ParentJobs, parent)
	job.Mu.Unlock()

	hasCycle, err := hasCycle(parent)
	if err != nil {
		parent.ChildJobs = parent.ChildJobs[:len(parent.ChildJobs)-1]
		parent.ParentJobs = parent.ParentJobs[:len(parent.ParentJobs)-1]
		return err
	}

	if hasCycle {
		parent.ChildJobs = parent.ChildJobs[:len(parent.ChildJobs)-1]
		job.ParentJobs = job.ParentJobs[:len(job.ParentJobs)-1]
		return errors.New("While adding " + parent.Name + " to " + job.Name + ": cycle not allowed")
	}

	return nil
}

func (parent *Job) Then(job *Job) error {
	if job.DagID != parent.DagID {
		return errors.New("cannot combine jobs of different DAGS")
	}

	parent.Mu.Lock()
	parent.ChildJobs = append(parent.ChildJobs, job)
	parent.Mu.Unlock()

	job.Mu.Lock()
	job.ParentJobs = append(job.ParentJobs, parent)
	job.Mu.Unlock()

	hasCycle, err := hasCycle(job)
	if err != nil {
		parent.ChildJobs = parent.ChildJobs[:len(parent.ChildJobs)-1]
		job.ParentJobs = job.ParentJobs[:len(job.ParentJobs)-1]
		return err
	}

	if hasCycle {
		parent.ChildJobs = parent.ChildJobs[:len(parent.ChildJobs)-1]
		job.ParentJobs = job.ParentJobs[:len(job.ParentJobs)-1]
		return errors.New("While adding " + job.Name + " to " + parent.Name + ": cycle not allowed")
	}

	return nil
}

func hasCycle(job *Job) (bool, error) {
	for _, parentJob := range job.ParentJobs {
		//fmt.Println(parentJob.Name, "==", job.Name)
		if job.ID == parentJob.ID {
			return false, errors.New("cycle not allowed")
		}

		isCycle, err := testForCycleParent(parentJob, job)
		if err != nil {
			return false, err
		}
		if isCycle {
			return true, nil
		}
	}

	return false, nil
}

func testForCycleParent(job *Job, origJob *Job) (bool, error) {

	for _, parentJob := range job.ParentJobs {
		//fmt.Println(parentJob.Name, "==", origJob.Name)
		if parentJob.ID == origJob.ID {
			return true, nil
		}
		isCycle, err := testForCycleParent(parentJob, origJob)
		if err != nil {
			return false, err
		}
		if isCycle {
			return true, nil
		}

	}

	return false, nil

}
