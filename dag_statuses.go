package dag

type DagStatus string

const (
	DagStatusNotStarted         DagStatus = "DagStatusNotStarted"
	DagStatusRunning            DagStatus = "DagStatusRunning"
	DagStatusFinishedSuccess    DagStatus = "DagStatusFinishedSuccess"
	DagStatusFinishedWithErrors DagStatus = "DagStatusFinishedWithErrors"
)

// GetPreV04DagStatusText returns the text status code from < v0.4.0.
// !!!! DO NOT USE THESE IN NEW CODE. !!!!
// It returns the empty string if the code is unknown.
func GetPreV04DagStatusText(status DagStatus) string {
	switch status {
	case DagStatusNotStarted:
		return "NOT_RUNNING"
	case DagStatusRunning:
		return "RUNNING"
	case DagStatusFinishedSuccess:
		return "FINISHED"
	case DagStatusFinishedWithErrors:
		return "FINISHED_WITH_ERRORS"
	default:
		return ""
	}
}
