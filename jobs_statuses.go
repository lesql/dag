package dag

import (
	"sync"
	"time"
)

type JobStatus string

const (
	JobStatusNotStarted                                JobStatus = "JobStatusNotStarted"
	JobStatusRunning                                   JobStatus = "JobStatusRunning"
	JobStatusFinishedSuccess                           JobStatus = "JobStatusFinishedSuccess"
	JobStatusFinishedConditionNotMet                   JobStatus = "JobStatusFinishedConditionNotMet"
	JobStatusFinishedWithSoftError                     JobStatus = "JobStatusFinishedWithSoftError"
	JobStatusFinishedWithErrorAsItHasParentsHaveErrors JobStatus = "JobStatusFinishedWithErrorAsItHasParentsHaveErrors"
	JobStatusFinishedWithError                         JobStatus = "JobStatusFinishedWithError"
)

// GetPreV04JobStatusText returns the text status code from < v0.4.0.
// !!!! DO NOT USE THESE IN NEW CODE. !!!!
// It returns the empty string if the code is unknown.
func GetPreV04JobStatusText(status JobStatus) string {
	switch status {
	case JobStatusNotStarted:
		return "NOT_STARTED"
	case JobStatusRunning:
		return "RUNNING"
	case JobStatusFinishedSuccess:
		return "FINISHED"
	case JobStatusFinishedConditionNotMet:
		return "CONDITION_NOT_MET"
	case JobStatusFinishedWithSoftError:
		return "FINISHED_WITH_SOFT_ERROR"
	case JobStatusFinishedWithErrorAsItHasParentsHaveErrors:
		return "FINISHED_WITH_ERRORS_AS_IT_HAS_PARENT_ERRORS"
	case JobStatusFinishedWithError:
		return "FINISHED_WITH_ERRORS"
	default:
		return ""
	}
}

type Job struct {
	ID         string
	Name       string
	task       func() (map[string]string, error)
	DagID      string
	ParentJobs []*Job
	ChildJobs  []*Job
	Status     JobStatus
	CreatedTs  time.Time
	StartedTs  time.Time
	FinishedTs time.Time
	Data       map[string]string
	Responses  map[string]string
	Logger     Logger
	Mu         sync.RWMutex
}
