package dag

import (
	"math/rand"
	"time"

	"filippo.io/mostly-harmless/cryptosource"
	"github.com/eknkc/basex"
	ulid "github.com/oklog/ulid/v2"
)

func randStringUlid() string {
	base62, _ := basex.NewEncoding("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	// //Sleep one nanosecond. Sometimes this function run that fast that we had the same entropy for multiple runs
	// time.Sleep(1 * time.Nanosecond)
	// rand.Seed(time.Now().UnixNano())

	//Use crypto/rand for seeding instead of time based rand
	// var b [8]byte
	// _, err := crypto_rand.Read(b[:])
	// if err != nil {
	// 	panic("cannot seed math/rand package with cryptographically secure random number generator")
	// }
	// rand.Seed(int64(binary.LittleEndian.Uint64(b[:])))
	//r := int64(rand.Int31())
	//t := time.Unix(r, 0)
	//entropy := rand.New(rand.NewSource(t.UnixNano()))

	entropy := rand.New(cryptosource.New())
	r := int64(entropy.Int31())
	t := time.Unix(r, 0)
	Ulid, _ := ulid.New(ulid.Timestamp(t), entropy)
	ulidByte, _ := Ulid.MarshalBinary()
	ID := base62.Encode(ulidByte)

	//fmt.Println(ID)

	/*
		log.Println(ulidByte)
		log.Println(Ulid)
		log.Println(ID)

		blub, _ := base62.Decode(ID)
		want := ulid.ULID{}
		want.UnmarshalBinary(blub)
		log.Println(want)
	*/

	return ID
}
