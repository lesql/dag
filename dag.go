package dag

import (
	"errors"
	"sync"
	"time"
)

var hooks Hooks

type Logger interface {
	Debug(args ...interface{})
	Debugf(format string, args ...interface{})
	Info(args ...interface{})
	Infof(format string, args ...interface{})
	Error(args ...interface{})
	Errorf(format string, args ...interface{})
}

// Dag represents directed acyclic graph
type Dag struct {
	ID         string
	Name       string
	Jobs       map[string]*Job
	JobsOrder  []string
	Status     DagStatus
	CreatedTs  time.Time
	StartedTs  time.Time
	FinishedTs time.Time
	Logger     Logger
	mu         sync.RWMutex
}

// Hooks define what happens on certain events like DAG start or JOB finished with error
type Hooks struct {
	DagCreated         func(dag *Dag)
	DagStarted         func(dag *Dag)
	DagFinishedSuccess func(dag *Dag)
	DagFinishedFailed  func(dag *Dag)
	JobCreated         func(job *Job)
	JobStarted         func(job *Job)
	JobFinishedSuccess func(job *Job)
	JobFinishedFailed  func(job *Job)
}

// New creates new DAG
func New(name string, lggr Logger, hooksLocal Hooks) (*Dag, error) {

	hooks = hooksLocal

	dagID := randStringUlid()

	jobs := make(map[string]*Job)

	dag := &Dag{
		ID:        dagID,
		Name:      name,
		Status:    DagStatusNotStarted,
		Jobs:      jobs,
		CreatedTs: time.Now(),
		Logger:    lggr,
	}

	hooks.DagCreated(dag)

	return dag, nil
}

// Run starts the DAG main loop which controls the jobs
func (dag *Dag) Run() error {

	//Prevent dag running in parallel
	if dag.Status == DagStatusRunning {
		dag.Logger.Error("DAG already running")
		return errors.New("DAG already running")
	} else if dag.Status == DagStatusFinishedSuccess {
		dag.Logger.Error("Cannot run DAG twice")
		return errors.New("cannot run DAG twice")
	}

	//Set dag status to running
	dag.Status = DagStatusRunning
	dag.StartedTs = time.Now()

	hooks.DagStarted(dag)

	dag.Logger.Debug(dag.ID)
	dag.Logger.Debug(dag.Name)

	//Start all jobs which don't have parents
	for _, job := range dag.Jobs {

		// Start job, if it has no parents and isn't running yet

		job.Mu.RLock()
		jobStatus := job.Status
		lenParentJobs := len(job.ParentJobs)
		job.Mu.RUnlock()

		if lenParentJobs == 0 && jobStatus == JobStatusNotStarted {
			go runJob(job)
		}

	}

	allJobsSuccessful := true

	//This is the DAGs mainloop, run this until all jobs are finished
	for {
		time.Sleep(1 * time.Second)

		allFinished := true

		for _, job := range dag.Jobs {

			//color.Blue(fmt.Sprintf("=> %s: Check status: %s", job.name, job.status))

			job.Mu.RLock()
			jobStatus := job.Status
			job.Mu.RUnlock()

			if jobStatus == JobStatusNotStarted {

				//atLeastOneParentJobFinishedWithErrors := false
				startJob := true
				for _, parentJob := range job.ParentJobs {

					parentJob.Mu.RLock()
					parentJobStatus := parentJob.Status
					parentJob.Mu.RUnlock()

					if parentJobStatus != JobStatusFinishedSuccess &&
						parentJobStatus != JobStatusFinishedConditionNotMet &&
						parentJobStatus != JobStatusFinishedWithSoftError {
						startJob = false
					}

					if parentJobStatus == JobStatusFinishedWithError || parentJobStatus == JobStatusFinishedWithErrorAsItHasParentsHaveErrors {
						dag.Logger.Debugf("=> %s: parent finished with errors: %s Failing this job as well", job.Name, parentJob.Name)
						go failJob(job)
						startJob = false
					}
				}

				if startJob {
					dag.Logger.Debugf("=> %s All parents finished => starting", job.Name)
					go runJob(job)
				}

			}

			//if at least one job is not finished, set allFinished back to false
			if jobStatus != JobStatusFinishedSuccess &&
				jobStatus != JobStatusFinishedConditionNotMet &&
				jobStatus != JobStatusFinishedWithError &&
				jobStatus != JobStatusFinishedWithSoftError &&
				jobStatus != JobStatusFinishedWithErrorAsItHasParentsHaveErrors {
				allFinished = false
			}

			//if job not successful, set allJobsSuccessful to false
			if jobStatus != JobStatusFinishedSuccess &&
				jobStatus != JobStatusNotStarted &&
				jobStatus != JobStatusRunning &&
				jobStatus != JobStatusFinishedWithSoftError &&
				jobStatus != JobStatusFinishedConditionNotMet {
				// fmt.Println("--------------")
				// fmt.Println("allJobsSuccessful = false")
				// fmt.Println(job.Status)
				// fmt.Printf("%+v\n", job)
				// fmt.Println("--------------")
				allJobsSuccessful = false
			}

		}

		//if all jobs finished, exit main dag loop
		if allFinished {

			//Set dag status to finished
			if allJobsSuccessful {
				dag.mu.Lock()
				dag.Status = DagStatusFinishedSuccess
				dag.FinishedTs = time.Now()
				dag.mu.Unlock()
				hooks.DagFinishedSuccess(dag)
			} else {
				dag.mu.Lock()
				dag.Status = DagStatusFinishedWithErrors
				dag.FinishedTs = time.Now()
				dag.mu.Unlock()
				hooks.DagFinishedFailed(dag)
			}

			break
		}
	}
	return nil
}
