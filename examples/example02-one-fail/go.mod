module gitlab.com/lesql/dag/examples/example02-one-fail

replace gitlab.com/lesql/dag => ../../

go 1.17

require (
	github.com/sirupsen/logrus v1.9.0
	gitlab.com/lesql/dag v0.0.0-00010101000000-000000000000
)

require (
	filippo.io/mostly-harmless/cryptosource v0.0.0-20220729104653-a94493175ac6 // indirect
	github.com/eknkc/basex v1.0.1 // indirect
	github.com/oklog/ulid/v2 v2.1.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.22.0 // indirect
	golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab // indirect
)
